<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DaftarController extends Controller
{
    public function Register()
    {
        return view('page.daftar');
    }
    public function submit(Request $request)
    {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        return view('page.welcome', compact('firstname', 'lastname'));
    }
}
